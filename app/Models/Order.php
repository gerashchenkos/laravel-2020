<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Cart;

class Order extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'cart_id', 'amount', 'status', 'payment_log_id'];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class, "cart_id", "id");
    }
}
