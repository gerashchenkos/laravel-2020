<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\RatesService;

class GetRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:uah';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get current date usd -> uah rate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RatesService $rates)
    {
        parent::__construct();
        $this->rates = $rates;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
        $result = (string) $res->getBody();
        $rate = json_decode($result, true)[0]["sale"];
        $this->rates->create(
            [
                "date" => date("Y-m-d"),
                "rate" => $rate
            ]
        );
        $this->info('Current USD -> UAH rate '. $rate . ' was sucessfully saved');
        return 0;
    }
}
