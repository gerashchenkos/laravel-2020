<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email {name?} {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send test email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name') ?? '';
        $email = $this->argument('email') ?? '';
        if (empty($name)) {
            $name = $this->ask('What is user\'s name?');
        }
        if (empty($email)) {
            $email = $this->ask('What is user\'s email?');
        }
        $i = 0;
        while (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Email is not valid!');
            $email = $this->ask('What is user\'s email?');
            $i ++;
            if ($i > 2) {
                $this->error('Error! You increased limit of email filling attempts');
                return 0;
            }
        }
        Mail::raw('Hi, welcome ' . $name .'!', function ($message) use ($email) {
            $message->to($email)
                ->subject("Test!");
        });
        $this->info('Email was successfully sent!');
        return 0;
    }
}
