<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Auth;

class AmountLimitException extends Exception
{
    public function report()
    {
        \Log::debug('Amount Limit exceeded for user #' . Auth::id());
    }
}
