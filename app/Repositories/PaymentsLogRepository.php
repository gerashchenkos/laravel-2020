<?php


namespace App\Repositories;

use App\Models\PaymentsLog;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class PaymentsLogRepository extends BaseRepository
{
    public function __construct(PaymentsLog $model)
    {
        $this->model = $model;
    }
}
