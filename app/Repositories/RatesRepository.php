<?php


namespace App\Repositories;

use App\Models\Rate;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class RatesRepository extends BaseRepository
{
    public function __construct(Rate $model)
    {
        $this->model = $model;
    }

    public function getLastRate(): float
    {
        return ($this->model->latest()->value('rate') ?? 1);
    }
}
