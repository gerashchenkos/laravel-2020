<?php


namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class ProductRepository extends BaseRepository
{
    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getByCategoryWithPagination(string $categoryId): LengthAwarePaginator
    {
        return $this->model->where("category_id", $categoryId)
            ->paginate($this->count);
    }

    public function search(string $name): Collection
    {
        return $this->model->where("name", 'like', '%' . $name.'%')->get();
    }
}
