<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\OrderRepository;
use App\Models\Cart;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Services\UserService;
use App\Models\User;
use App\Services\CartService;
use App\Services\PaymentsLogService;

class OrderService extends BaseService
{
    private $stripeRequest = [];

    public function __construct(
        OrderRepository $repo,
        UserService $users,
        CartService $cart,
        PaymentsLogService $paymentsLog
    ) {
        $this->repo = $repo;
        $this->users = $users;
        $this->cart = $cart;
        $this->paymentsLog = $paymentsLog;
    }

    private function setStripeRequest(array $logRequest = [])
    {
        $this->stripeRequest = $logRequest;
    }

    public function pay(User $user, Cart $cart, array $creditCard): array
    {
        if (empty($user->stripe_id)) {
            $customer = Stripe::customers()->create(
                [
                    'email' => $user->email,
                ]
            );
            $customerId = $customer['id'];
            $this->users->update(
                $user->id,
                [
                    "stripe_id" => $customer['id']
                ]
            );
        } else {
            $customerId = $user->stripe_id;
        }
        $token = Stripe::tokens()->create(
            [
                'card' => [
                    'number' => $creditCard['number'],
                    'exp_month' => explode("/", $creditCard['expiration'])[0],
                    'cvc' => $creditCard['cvv'],
                    'exp_year' => explode("/", $creditCard['expiration'])[1],
                ],
            ]
        );

        try {
            $this->setStripeRequest(
                [
                    "request_type" => "card_add",
                    "customer_id" => $customerId,
                    "token" => $token['id']
                ]
            );
            $card = Stripe::cards()->create($customerId, $token['id']);
            $this->setStripeRequest(
                [
                    "request_type" => "customer_update",
                    "customer_id" => $customerId,
                    "default_source" => $card['id']
                ]
            );
            $customer = Stripe::customers()->update(
                $customerId,
                [
                    'default_source' => $card['id'],
                ]
            );
            $amount = $this->cart->calTotalProductsPrice($cart->id);
            $chargeRequest = [
                'customer' => $customerId,
                'currency' => 'USD',
                'amount' => $amount,
                'capture' => true
            ];
            $this->setStripeRequest(
                $chargeRequest
            );
            $charge = Stripe::charges()->create(
                $chargeRequest
            );
        } catch (
            \Cartalyst\Stripe\Exception\BadRequestException
            | \Cartalyst\Stripe\Exception\InvalidRequestException
            | \Cartalyst\Stripe\Exception\CardErrorException
            | \Cartalyst\Stripe\Exception\ServerErrorException $e
        ) {
            // Get the status code
            $code = $e->getCode();
            // Get the error message returned by Stripe
            $message = $e->getMessage();
            // Get the error type returned by Stripe
            $type = $e->getErrorType();
            $paymentsLog = $this->paymentsLog->create(
                [
                    "user_id" => $user->id,
                    "request" => json_encode($this->stripeRequest),
                    "response" => json_encode(
                        [
                            "code" => $code,
                            "message" => $message,
                            "type" => $type
                        ]
                    ),
                    "success" => false
                ]
            );
            return ["success" => 0, "error" => $message];
        }
        $order = $this->create(
            [
                "user_id" => $user->id,
                "cart_id" => $cart->id,
                "amount" => $amount,
                "status" => "Paid"
            ]
        );
        $paymentsLog = $this->paymentsLog->create(
            [
                "user_id" => $user->id,
                "request" => json_encode($chargeRequest),
                "response" => json_encode($charge),
                "success" => true
            ]
        );
        $this->update($order->id, ["payment_log_id" => $paymentsLog->id]);
        return ["success" => 1, "order" => $order->id];
    }
}
