<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Support\Collection;
use App\Services\LangService;
use Illuminate\Database\Eloquent\Model;

class UserService extends BaseService
{
    public function __construct(UserRepository $repo, LangService $langs)
    {
        $this->repo = $repo;
        $this->langs = $langs;
    }

    public function activeUsersCount(): int
    {
        return $this->repo->activeUsersCount();
    }

    public function getAllNotAdmins(): Collection
    {
        return $this->repo->getAllNotAdmins();
    }

    public function update(string $id, array $data): bool
    {
        if (!empty($data['lang'])) {
            $langId = $this->langs->getIdByShortName($data['lang']);
            $data['lang_id'] = $langId;
        } else {
            $data['lang_id'] = null;
        }
        unset($data['lang']);
        return parent::update($id, $data);
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function create(array $data): Model
    {
        if (!empty($data['lang'])) {
            $langId = $this->langs->getIdByShortName($data['lang']);
            $data['lang_id'] = $langId;
        }
        unset($data['lang']);
        $data['password'] = Hash::make($data['password']);
        $data['type'] = 'user';
        return parent::create($data);
    }
}
