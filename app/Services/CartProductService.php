<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CartProductRepository;

class CartProductService extends BaseService
{
    public function __construct(CartProductRepository $repo)
    {
        $this->repo = $repo;
    }
}
