<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\RatesRepository;

class RatesService extends BaseService
{
    public function __construct(RatesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getLastRate(): float
    {
        return $this->repo->getLastRate();
    }
}
