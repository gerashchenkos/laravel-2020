<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CartRepository;
use App\Exceptions\AmountLimitException;

class CartService extends BaseService
{
    public function __construct(CartRepository $repo)
    {
        $this->repo = $repo;
    }

    public function calTotalProductsPrice(string $id): float
    {
        $cart = $this->repo->findById($id);
        $sum = 0;
        foreach ($cart->products as $product) {
            $sum += $product->price * $product->pivot->quantity;
        }
        /*if ($sum > env("AMOUNT_LIMIT")) {
            throw new AmountLimitException("Your cart amount is too big. It must be < " . env("AMOUNT_LIMIT"));
        }*/
        return $sum;
    }

    public function clearCart(string $id)
    {
        foreach ($this->repo->findById($id)->products as $product) {
            $product->pivot->delete();
        }
        return true;
    }
}
