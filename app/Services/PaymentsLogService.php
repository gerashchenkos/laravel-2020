<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\PaymentsLogRepository;

class PaymentsLogService extends BaseService
{
    public function __construct(PaymentsLogRepository $repo)
    {
        $this->repo = $repo;
    }
}
