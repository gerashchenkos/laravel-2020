<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;
use App\Http\Requests\ProductStoreRequest;

class AdminProductController extends Controller
{
    public function __construct(ProductService $products, CategoryService $categories)
    {
        $this->products = $products;
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'admin_product.index',
            ["products" => $this->products->all(),]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'admin_product.create',
            [
                "categories" => $this->categories->all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product = $this->products->create($request->all());
        return redirect()->route('products.show', ["product" => $product->id])->withSuccess(
            'Product #' . $product->id . " was created!"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view(
            'admin_product.show',
            ["product" => $this->products->findById($id)]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(
            'admin_product.edit',
            [
                "product" => $this->products->findById($id),
                "categories" => $this->categories->all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        $this->products->update($product->id, $request->except(['_method', '_token']));
        return redirect()->route('products.show', ["product" => $product->id])->withSuccess(
            'Product #' . $product->id . " was updated!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->products->destroy($id);
        return redirect()->route('products.index')->withSuccess('Product #' . $id . " was deleted!");
    }
}
