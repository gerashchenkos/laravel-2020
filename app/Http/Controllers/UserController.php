<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Lang;
use App\Models\User;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Services\LangService;
use App\Services\UserService;

class UserController extends Controller
{
    public function __construct(LangService $lang, UserService $user)
    {
        $this->lang = $lang;
        $this->user = $user;
    }

    public function profile(Request $request)
    {
        return view(
            'user.profile',
            [
                "user" => Auth::user(),
                "langs" => $this->lang->all()
            ]
        );
    }

    public function update(UpdateUserProfileRequest $request)
    {
        $this->user->update(
            Auth::id(),
            [
                "name" => $request->input('name'),
                "lang_id" => $this->lang->getIdByShortName($request->input('lang'))
            ]
        );

        return redirect()->route('profile');
    }
}
