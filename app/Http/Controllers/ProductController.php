<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;
use App\Models\CartProducts;
use App\Models\Category;
use Helper;
use App\Services\ProductService;
use App\Services\CategoryService;
use Mpdf\Mpdf;
use App\Services\RatesService;
use View;

class ProductController extends Controller
{
    public function __construct(ProductService $products, CategoryService $categories, RatesService $rates)
    {
        $this->products = $products;
        $this->categories = $categories;
        $this->rates = $rates;
    }

    public function index(Request $request, $categoryId = null)
    {
        if (empty($categoryId)) {
            $products = $this->products->getAllWithPagination();
        } else {
            $products = $this->products->getByCategoryWithPagination($categoryId);
        }
        $categories = $this->categories->all();
        $title = Helper::shout("Product page");
        return view(
            'products.index',
            [
                "products" => $products,
                "categories" => $categories,
                "categoryId" => $categoryId,
                "rate" => $this->rates->getLastRate(),
            ]
        );
    }

    public function search(Request $request)
    {
        /*$view = View::make(
            'products.search',
            [
                "products" => $this->products->search($request->name),
                "rate" => $this->rates->getLastRate(),
            ]
        );
        $html = $view->render();
        return $html;
        */

        return view(
            'products.search',
            [
                "products" => $this->products->search($request->name),
                "rate" => $this->rates->getLastRate(),
            ]
        );
    }
}
