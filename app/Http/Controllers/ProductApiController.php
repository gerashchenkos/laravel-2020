<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Services\RatesService;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;

class ProductApiController extends Controller
{
    public function __construct(ProductService $products)
    {
        $this->products = $products;
    }

    public function index(Request $request, $categoryId = null)
    {
        if (empty($categoryId)) {
            $products = $this->products->getAllWithPagination();
        } else {
            $products = $this->products->getByCategoryWithPagination($categoryId);
        }
        return ProductResource::collection($products);
    }
}
