<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Services\UserService;

class AdminMainController extends Controller
{
    public function __construct(ProductService $products, CategoryService $categories, UserService $users)
    {
        $this->products = $products;
        $this->categories = $categories;
        $this->users = $users;
    }

    public function index(Request $request)
    {
        return view(
            'admin.admin_main',
            [
                "productsCount" => $this->products->count(),
                "categoriesCount" => $this->categories->count(),
                "usersCount" => $this->users->activeUsersCount()
            ]
        );
    }
}
