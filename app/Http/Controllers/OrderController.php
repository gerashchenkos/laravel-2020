<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\CartService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PayRequest;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Services\UserService;
use App\Services\OrderService;
use View;
use Mpdf\Mpdf;

class OrderController extends Controller
{
    public function __construct(CartService $cart, UserService $users, OrderService $orders)
    {
        $this->cart = $cart;
        $this->users = $users;
        $this->orders = $orders;
    }

    public function index(Request $request)
    {
        $cart = $this->cart->findById(session('cart_id') ?? 0);
        return view(
            'order.index',
            [
                "cart" => $cart,
                "totalPrice" => $this->cart->calTotalProductsPrice($cart->id),
                "user" => Auth::user()
            ]
        );
    }

    public function pay(PayRequest $request)
    {
        $resp = $this->orders->pay(
            Auth::user(),
            $this->cart->findById(session('cart_id')),
            $request->all()
        );
        if ($resp['success'] == 1) {
            return redirect()->route("order.thankyou", ["order" => $resp['order']]);
        } else {
            return redirect()->back()->with('error', $resp['error']);
        }
    }

    public function thankyou(Request $request)
    {
        return view(
            'order.thankyou',
            [
                "order" => $this->orders->findById($order),
            ]
        );
    }

    public function invoicePdf(Request $request)
    {
        $cart = $this->cart->findById(session('cart_id') ?? 0);
        $view = View::make(
            'order.invoicepdf',
            [
                "cart" => $cart,
                "totalPrice" => $this->cart->calTotalProductsPrice($cart->id),
                "user" => Auth::user()
            ]
        );
        $html = $view->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output("invoice.pdf", "D");
    }
}
