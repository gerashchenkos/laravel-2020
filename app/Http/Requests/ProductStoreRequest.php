<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                "max:255",
                "min:3",
                Rule::unique('products')
                    ->where('category_id', $this->category_id)
            ],
            'price' => 'required|numeric|between:1,100000',
            'quantity' => 'required|integer|between:1,10000',
            'category_id' => 'required|integer|exists:categories,id',
        ];
    }
}
