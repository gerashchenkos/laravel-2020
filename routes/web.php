<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminMainController;
use App\Http\Controllers\AdminProductController;
use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth', 'user.check'])->group(function () {
    Route::get('/{category_id?}', [ProductController::class, 'index'])->name('products')->where('category_id', '[0-9]+');
    Route::post('/products/search', [ProductController::class, 'search'])->name('products.search');

    Route::prefix('profile')->group(
        function () {
                Route::get('/', [UserController::class, 'profile'])->name('profile');
                Route::post('/update', [UserController::class, 'update'])->name('profile.update');
            }
    );

    Route::prefix('cart')->group(
        function () {
                Route::get('/', [CartController::class, 'index'])
                    ->name('cart')->middleware('cart.exist');

                Route::post('/add', [CartController::class, 'add'])
                    ->name('cart.add');
            }
    );

    Route::prefix('order')->group(
        function () {
            Route::get('/', [OrderController::class, 'index'])
                ->name('order')->middleware('cart.exist');
            Route::post('/pay', [OrderController::class, 'pay'])
                ->name('order.pay')->middleware('cart.exist');
            Route::get('/thankyou/{order}', [OrderController::class, 'thankyou'])
                ->name('order.thankyou');
            Route::get('/invoice_pdf', [OrderController::class, 'invoicePdf'])
                ->name('order.invoicepdf')->middleware('cart.exist');
        }
    );
});

Route::middleware(['auth', 'admin.check'])->group(function () {
    Route::prefix('admin')->group(
        function () {
            Route::get('/', [AdminMainController::class, 'index'])->name('admin');
            Route::resource('products', AdminProductController::class);
            Route::resource('categories', AdminCategoryController::class);
            Route::resource('users', AdminUserController::class);
        }
    );
});

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
