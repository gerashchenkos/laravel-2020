<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    public function testLoginCsrf()
    {
        $response = $this->post('/login', ["email" => "test@email.com", "password" => "1111"]);

        $response->assertStatus(302);
    }

    public function testLoginForm()
    {
        $response = $this->get('/login');
        $response->assertSee('<input id="email" type="email" class="form-control " name="email" value="" required autocomplete="email" autofocus>', $escaped = false);
    }

    public function testMainPage()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
    }

    public function testMainPageProductExists()
    {
        $user = User::factory()->create();
        Category::factory()->create();
        Product::factory()->create();
        $response = $this->actingAs($user)->get('/');
        $response->assertSee('<input type="checkbox" name="products[]"', $escaped = false);
    }

    public function testLoginFormValidationError()
    {
        $view = $this->withViewErrors(
            [
                'email' => ['Please provide a valid email.']
            ]
        )->view('auth.login');

        $view->assertSee('Please provide a valid email.');
    }

    public function testSuccessSendEmailCommand()
    {
        $this->artisan('send:email')
            ->expectsQuestion('What is user\'s name?', 'Test')
            ->expectsQuestion('What is user\'s email?', 'e@e.com')
            ->expectsOutput('Email was successfully sent!')
            ->assertExitCode(0);
    }

    public function testFailSendEmailCommand()
    {
        $this->artisan('send:email')
            ->expectsQuestion('What is user\'s name?', 'Test')
            ->expectsQuestion('What is user\'s email?', 'e.com')
            ->expectsQuestion('What is user\'s email?', 'e.com')
            ->expectsQuestion('What is user\'s email?', 'e.com')
            ->expectsOutput('Email was successfully sent!')
            ->assertExitCode(0);
    }
}
