<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Lang;

class LangSeeder extends Seeder
{
    private $langs = ["RU" => "Russian", "UA" => "Ukraine", "EN" => "English", "DE" => "German"];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->langs as $code => $lang) {
            Lang::create(
                [
                    "full_name" => $lang,
                    "short_name" => $code
                ]
            );
        }
    }
}
