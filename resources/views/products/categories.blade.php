<div>
    <a href="{{route('products')}}">
        @if(empty($categoryId))
            <b>All</b>
        @else
            All
        @endif
    </a><br>
    @foreach ($categories as $cat)
        <a href="{{route('products', ["category_id" => $cat->id])}}">
            @if($cat->id == $categoryId)
                <b>{{ $cat->name }}</b>
            @else
                {{ $cat->name }}
            @endif
        </a>
        <br>
    @endforeach
</div>
