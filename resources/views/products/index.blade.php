@extends('layouts.main')

@section('content')
    <div class="album py-5 bg-light">
        @if( session('error'))
            <div class="alert alert-danger alert-dismissible">
                <h5><i class="icon fas fa-check"></i> Alert!</h5>
                {{ session('error') }}
            </div>
        @endif
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="row">
                    <div class="col-md-2">
                        @include('products.categories', ['categories' => $categories, 'categoryId' => $categoryId])
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Product Name" aria-label="Product Name" name="search" id="search" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" onclick="search()" type="button">Find</button>
                                </div>
                            </div>
                        </div>
                        <form method="post" action="{{ route('cart.add') }}">
                            @csrf
                            <div class="row" id="products">
                                @foreach ($products as $product)
                                    @include('products.product', ['product' => $product, 'rate' => $rate])
                                @endforeach
                            </div>
                            <input type="submit" class="btn btn-success" value="Order">
                        </form>
                        <br/>
                        {{ $products->links() }}
                    </div>
                </div>
        </div>
    </div>

    <script>
        function search()
        {
            $("#products").html('<div class="spinner-border"></div>');
            $(".pagination").remove();
            const name = document.getElementById('search').value;
            $.ajax({
                  method: "POST",
                  url: "{{ route('products.search') }}",
                  data: { "_token": "{{ csrf_token() }}", "name": name }
            }).done(function( msg ) {
               $("#products").html(msg);
            });

        }
    </script>
@endsection
