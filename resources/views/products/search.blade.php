@foreach ($products as $product)
    @include('products.product', ['product' => $product, 'rate' => $rate])
@endforeach