<div class="col-md-4">
    <div class="card mb-4 shadow-sm">
        @if(!empty($product->image))
            <img class="img-fluid product-img" src="{{ $product->image }}">
        @else
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                 xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                 focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>
                    Placeholder</title>
                <rect width="100%" height="100%" fill="#55595c"/>
                <text x="50%" y="50%" fill="#eceeef"
                      dy=".3em">{{ $product->name }}</text>
            </svg>
        @endif
        <div class="card-body">
            <p class="card-text">{{ $product->name . " (" . $product->quantity . ")" }}</p>
            <div class="d-flex justify-content-between align-items-center">
                BUY?<input type="checkbox" name="products[]"
                           value="{{ $product->id }}">
                <input type="number" id="quantity"
                       name="quantity_{{ $product->id }}" min="1"
                       max="{{ $product->quantity }}">
                <small class="text-muted">UAH @convert($product->price * $rate)</small>
            </div>
        </div>
    </div>
</div>
