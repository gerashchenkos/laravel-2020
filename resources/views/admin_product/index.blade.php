@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <table id="products" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example2_info">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending">Name
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Price
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Quantity
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Category
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Actions
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            <tr role="row">
                <td>{{ $product->name }}</td>
                <td>$@convert($product->price)</td>
                <td>{{ $product->quantity }}</td>
                <td>{{ $product->category->name }}</td>
                <td>
                    <a class="btn btn-primary btn-sm w-50" href="{{ route('products.show', ["product" => $product->id]) }}">
                        <i class="fas fa-folder">
                        </i>
                        View
                    </a> <br>
                    <a class="btn btn-info btn-sm w-50" href="{{ route('products.edit', ["product" => $product->id]) }}">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </a>
                    <form method="POST" action="{{ route('products.destroy', ["product" => $product->id]) }}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm w-50">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
        </table>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#products').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@stop
