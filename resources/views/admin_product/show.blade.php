@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
        <div class="card">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid" src="{{ $product->image }}" alt="Product picture">
                </div>

                <h3 class="profile-username text-center">#{{ $product->id }} {{ $product->name }}</h3>

                <p class="text-muted text-center">{{ $product->category->name }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Price</b> <a class="float-right">@convert($product->price)</a>
                    </li>
                    <li class="list-group-item">
                        <b>Quantity</b> <a class="float-right">{{$product->quantity}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Creation Date</b> <a class="float-right">{{$product->created_at}}</a>
                    </li>
                </ul>

                <a href="{{route('products.edit', ["product" => $product->id])}}" class="btn btn-primary btn-block"><b>Edit</b></a>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
@stop

@section('js')
@stop
