@extends('layouts.main')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="card-header">{{ __('Edit Profile') }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('profile.update') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email:') }}</label>
                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control" name="email" value="{{$user->email}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{old('name') ?? $user->name}}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lang" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>

                        <div class="col-md-6">
                            <select id="lang" class="form-control @error('lang') is-invalid @enderror" name="lang">
                                @foreach($langs as $lang)
                                    <option @if($user->lang->short_name == $lang->short_name) selected
                                            @endif value="{{$lang->short_name}}">{{$lang->full_name}}</option>
                                @endforeach
                            </select>
                            @error('lang')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Edit') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
