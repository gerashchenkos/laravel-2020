@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <table id="categories" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example2_info">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending">ID
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Name
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Created
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Actions
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr role="row">
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->created_at }}</td>
                <td>
                    <a class="btn btn-primary btn-sm w-40" href="{{ route('categories.show', ["category" => $category->id]) }}">
                        <i class="fas fa-folder">
                        </i>
                        View
                    </a>
                    <a class="btn btn-info btn-sm w-40" href="{{ route('categories.edit', ["category" => $category->id]) }}">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
        </table>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#categories').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@stop
