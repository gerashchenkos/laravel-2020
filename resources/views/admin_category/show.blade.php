@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
        <div class="card">
            <div class="card-body box-profile">
                <h3 class="profile-username text-center">#{{ $category->id }} {{ $category->name }}</h3>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Creation Date</b> <a class="float-right">{{$category->created_at}}</a>
                    </li>
                </ul>

                <a href="{{route('categories.edit', ["category" => $category->id])}}" class="btn btn-primary btn-block"><b>Edit</b></a>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
@stop

@section('js')
@stop
