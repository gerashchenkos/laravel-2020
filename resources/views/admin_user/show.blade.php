@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
        <div class="card">
            <div class="card-body box-profile">

                <h3 class="profile-username text-center">#{{ $user->id }} {{ $user->name }}</h3>

                <p class="text-muted text-center">{{ $user->email }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Status</b> <a class="float-right">{{$user->status}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Lang</b> <a class="float-right">{{$user->lang->full_name}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Creation Date</b> <a class="float-right">{{$user->created_at}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Email Verified</b> <a class="float-right">{{$user->email_verified_at}}</a>
                    </li>
                </ul>

                <a href="{{route('users.edit', ["user" => $user->id])}}" class="btn btn-primary btn-block"><b>Edit</b></a>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
@stop

@section('js')
@stop
