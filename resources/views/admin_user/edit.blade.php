@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('users.update', ["user" => $user->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Users Data</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" name="name" id="inputName" class="form-control @error('name') is-invalid @enderror" value="{{old('name') ?? $user->name }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Email</label>
                            <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email') ?? $user->email }}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputStatus">Lang</label>
                            <select name="lang" class="form-control custom-select @error('lang') is-invalid @enderror">
                                <option value="">Select Lang</option>
                                @foreach($langs as $lang)
                                    <option @if(old('lang') && $lang->short_name == old('lang'))
                                                selected
                                            @elseif( empty(old('lang')) && $lang->short_name === $user->lang->short_name)
                                                selected
                                            @endif
                                            value="{{ $lang->short_name }}">{{ $lang->full_name }}</option>
                                @endforeach
                            </select>
                            @error('lang')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputStatus">Status</label>
                            <select name="status" class="form-control custom-select @error('status') is-invalid @enderror">
                                <option @if(old('status') && "active" == old('status'))
                                        selected
                                        @elseif( empty(old('status')) && "active" === $user->status)
                                        selected
                                        @endif
                                        value="active">Active</option>
                                <option @if(old('lang') && "not active" == old('status'))
                                        selected
                                        @elseif( empty(old('lang')) && "not active" === $user->status)
                                        selected
                                        @endif
                                        value="not active">Not Active</option>
                            </select>
                            @error('status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Save Changes" class="btn btn-success float-right">
            </div>
        </div>
    </form>
@stop

@section('js')
@stop
