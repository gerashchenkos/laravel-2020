@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <table id="users" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example2_info">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending">Name
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Email
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Status
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Lang
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Actions
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr role="row">
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->status }}</td>
                <td>
                    {{$user->lang->full_name}}
                </td>
                <td>
                    <a class="btn btn-primary btn-sm w-50" href="{{ route('users.show', ["user" => $user->id]) }}">
                        <i class="fas fa-folder">
                        </i>
                        View
                    </a> <br>
                    <a class="btn btn-info btn-sm w-50" href="{{ route('users.edit', ["user" => $user->id]) }}">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </a>
                    <form method="POST" action="{{ route('users.destroy', ["user" => $user->id]) }}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm w-50">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
        </table>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#users').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@stop
