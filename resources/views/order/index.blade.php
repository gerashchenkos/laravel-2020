@extends('layouts.main')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="invoice p-3 mb-3">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> Best Shop, Inc.
                            <small class="float-right">Date: {{ date('m/d/Y') }}</small>
                        </h4>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        From
                        <address>
                            <strong>Best Shop, Inc.</strong><br>
                            795 Folsom Ave, Suite 600<br>
                            San Francisco, CA 94107<br>
                            Phone: (804) 123-5432<br>
                            Email: info@shop.com
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        To
                        <address>
                            <strong>{{ $user->name }}</strong><br>
                            Email: {{ $user->email }}
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>Invoice #{{ $cart->id }}</b><br>
                        <br>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Serial #</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($cart->products as $product)
                                <tr>
                                    <td>{{ $product->pivot->quantity }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->id }}</td>
                                    <td>$@convert($product->price * $product->pivot->quantity)</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-6">
                        <p class="lead">Payment Methods:</p>
                        <img src="/images/visa.png" alt="Visa">
                        <img src="/images/mastercard.png" alt="Mastercard">
                        <img src="/images/american-express.png" alt="American Express">

                        <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango
                            imeem
                            plugg
                            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                        </p>
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <p class="lead">Amount Due {{ date('m/d/Y') }}</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width:50%">Subtotal:</th>
                                    <td>$@convert($totalPrice)</td>
                                </tr>
                                <tr>
                                    <th>Shipping:</th>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td>$@convert($totalPrice)</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                    <div class="col-12">
                        <a href="invoice-print.html" target="_blank" class="btn btn-default"><i
                                class="fas fa-print"></i> Print</a>
                        <a href="{{ route('order.invoicepdf') }}" class="btn btn-primary float-right" style="margin-right: 5px;">
                            <i class="fas fa-download"></i> Generate PDF
                        </a>
                    </div>
                </div>

                <div class="col-md-12">
                    @if( session('error'))
                        <div class="alert alert-danger alert-dismissible">
                            <h5><i class="icon fas fa-check"></i> Alert!</h5>
                            {{ session('error') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('order.pay') }}">
                        @csrf
                        <h4 class="mb-3">Payment</h4>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="cc-number">Credit card number</label>
                                <input type="text" name="number" class="form-control @error('number') is-invalid @enderror" id="cc-number" placeholder=""
                                       required="">
                                @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="cc-expiration">Expiration (e.g. 12/21)</label>
                                <input type="text" name="expiration" class="form-control @error('expiration') is-invalid @enderror" id="cc-expiration"
                                       placeholder="MM/YY" required="">
                                @error('expiration')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="cc-expiration">CVV</label>
                                <input type="text" name="cvv" class="form-control @error('cvv') is-invalid @enderror" id="cc-cvv" placeholder=""
                                       required="">
                                @error('cvv')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-success float-left"><i class="far fa-credit-card"></i>
                                Submit
                                Payment
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
