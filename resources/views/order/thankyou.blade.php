@extends('layouts.main')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <h2>Thank you! We received payment ${{ $order->amount }}. Your order ID {{ $order->id }}.</h2>
        </div>
    </div>
@endsection
