<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <h2>Best Shop, Inc.</h2>
        <h3>Date: {{ date('m/d/Y') }}</h3>
        <table style="width: 100%">
            <tr>
                <td>
                    From
                </td>
                <td>
                    To
                </td>
                <td>
                    Invoice #{{ $cart->id }}
                </td>
            </tr>
            <tr>
                <td>
                    Best Shop, Inc.
                </td>
                <td>
                    <strong>{{ $user->name }}</strong>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    795 Folsom Ave, Suite 600
                </td>
                <td>
                    Email: {{ $user->email }}
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    San Francisco, CA 94107<br>
                    Phone: (804) 123-5432<br>
                    Email: info@shop.com
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <br>
        <table style="width: 100%">
            <tr>
                <td>
                    <strong>Qty</strong>
                </td>
                <td>
                    <strong>Product</strong>
                </td>
                <td>
                    <strong>Serial #</strong>
                </td>
                <td>
                    <strong>Subtotal</strong>
                </td>
            </tr>
            @foreach ($cart->products as $product)
                <tr>
                    <td>{{ $product->pivot->quantity }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->id }}</td>
                    <td>$@convert($product->price * $product->pivot->quantity)</td>
                </tr>
            @endforeach
        </table>
        <table style="width: 100%; margin-left: 45%">
            <tr>
                <td>
                    <strong>Amount Due {{ date('m/d/Y') }}</strong>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Subtotal:</strong>
                </td>
                <td>
                    $@convert($totalPrice)
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Shipping:</strong>
                </td>
                <td>
                    $0
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Total:</strong>
                </td>
                <td>
                    $@convert($totalPrice)
                </td>
            </tr>
        </table>
    </body>
</html>
